#!/bin/sh
. ./.production.env

TMPDIR="/tmp/conex"
mkdir -p "$TMPDIR"

query_db() {
    printf '%s\n' "$(PGPASSWORD=$DB_PASS psql -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -t -A -P pager=off -P border=0 -P format=unaligned -P tuples_only=on -c "$1")"
}

query_db "SELECT * FROM sites WHERE status = 'down' OR status = 'diff';" | while IFS='|' read -r _ directory _ _ _ _ _ _ _ title slogan banner json_data _ _ tags; do
    rm -f "$TMPDIR/site.md" "$TMPDIR/site.json"
    printf '%s' "$json_data" > "$TMPDIR/site.json"
    json2md -j "$TMPDIR/site.json" -t markdown -o "$TMPDIR/site.md"
    markdown_data="$(cat "$TMPDIR/site.md")"
    rm -f "$TMPDIR/site.md" "$TMPDIR/site.json"

    mkdir -p "content/$directory"
    printf '%s\ndate: %s\ntitle: %s\ndescription: %s\ntags: %s\nbanner: %s\nlayout: single\n%s\n\n%s\n' \
        '---' "$(date -I)" "$title" "$slogan" "$tags" "$banner" '---' "$markdown_data" > "content/$directory/_index.md"
done
