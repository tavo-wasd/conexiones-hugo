document.addEventListener("DOMContentLoaded", () => {
  const removeClass = className => Array.from(document.getElementsByClassName(className)).forEach(e => e.classList.remove(className));
  removeClass("js-only");

  const cards = document.querySelectorAll("#cardscontainer a");
  const search = document.getElementById("search");
  const cardscontainer = document.getElementById("cardscontainer");

  search.addEventListener("input", () => {
    const normalizeText = text => text.toLowerCase().trim().normalize('NFD').replace(/\p{Diacritic}/gu, "");
    const searchTerms = normalizeText(search.value).split(" ");
    const hasFilter = searchTerms.length > 0;

    cardscontainer.classList.toggle("list-searched", hasFilter);

    cards.forEach(card => {
      const cardText = normalizeText(`${card.textContent} ${card.dataset.tags}`);
      const isMatch = searchTerms.every(term => cardText.includes(term));

      card.hidden = !isMatch;
      card.classList.toggle("matched-card", hasFilter && isMatch);
    });
  });
});
