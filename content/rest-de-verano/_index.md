---
date: 2024-09-22
title: Rest. de Verano
description: Restaurante de comida asiática orientado a la familia
banner: https://r2.conex.one/rest-de-verano/2024-09-22-20-44-49-banner.jpg
tags: comidas chinos chinas asiáticos cartago arroz restaurantes cantonés
layout: single
---

# Contacto

- Tel: <a href="tel:+50625511453">2551 1453</a>
- Cel/Whatsapp: <a href="tel:+50687285146">8728 5146</a>

# Menú

![Bebidas](https://r2.conex.one/rest-de-verano/2024-09-22-20-45-10-platos1.jpg)

![Menu 1<br>](https://r2.conex.one/rest-de-verano/2024-09-22-20-45-25-menu1.jpg)

![Platos 1<br>](https://r2.conex.one/rest-de-verano/2024-09-22-20-45-39-platos2.jpg)

![Menu 2<br>](https://r2.conex.one/rest-de-verano/2024-09-22-20-46-09-menu2.jpg)

![Platos 2<br>](https://r2.conex.one/rest-de-verano/2024-09-22-20-46-20-platos3.jpg)

![Menu 3<br>](https://r2.conex.one/rest-de-verano/2024-09-22-20-46-29-menu3.jpg)
