---
date: 2024-09-22
title: Jóvenes Tutores
description: Tutorías virtuales de inglés, francés, primaria, secundaria.
banner: https://editor.conex.one/static/svg/banner.svg
tags: tutorias escuelas tutora ingles frances pŕimaria secundaria idiomas materias basicas estudiantes
layout: single
---

# Hilary Soto

- Cel/Tel: <a href="tel:+50672132335">7213 2335</a>
- Email: <a href="mailto:hilarysotod@gmail.com">hilarysotod@gmail.com</a>
