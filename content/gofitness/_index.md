---
date: 2024-09-22
title: GoFitness
description: Familia dedicada al bienestar, nos concentramos en ofrecer entrenamientos alternativos
banner: https://r2.conex.one/gofitness/2024-09-22-20-31-29-banner.jpg
tags: gyms gimnasios entrenamientos cartago pesas box acrobacias telas spinning escaladas
layout: single
---

![Horario](https://r2.conex.one/gofitness/2024-09-22-20-33-06-horario.jpg)

# Estamos a tu servicio

¡Únete a nosotros y descubre lo que es posible con un
entrenamiento adecuado y un compromiso constante! Para
cotizar otras disciplinas como trampolines, pared de
escalada, acrotelas, etc. se puede comunicar al
Whatsapp, con mucho gusto lo atenderemos!

# Reservaciones y agenda de citas

- Tel/Whatsapp: <a href="tel:+50661590074">6159 0074</a>

# Planes de entrenamiento

![Planes](https://r2.conex.one/gofitness/2024-09-22-20-32-30-plan.jpg)

- 2 Entrenos/Semana: ₡15 000
- 3 Entrenos/Semana ₡20 000
- Plan Ilimitado ₡25 000

# Acerca de GoFitness

GoFitness es un gimnasio dedicado al bienestar físico y
mental de nuestros miembros.
Ofrecemos una amplia variedad de equipos de última generación y
programas de entrenamiento diseñados por expertos en la materia.
Nos enfocamos en brindar una experiencia personalizada y única a
cada uno de nuestros miembros.

![Ejercicios<br>](https://r2.conex.one/gofitness/2024-09-22-20-34-14-publi.jpg)
